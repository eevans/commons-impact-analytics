package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopWikisByCategoryLogicInterface interface {
	ProcessTopWikisByCategoryLogic(parameters entities.TopWikisByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByCategoryResponse)
}

type TopWikisByCategoryLogic struct {
	Data data.TopWikisByCategoryDataInterface
}

func NewTopWikisByCategoryLogic(data data.TopWikisByCategoryDataInterface) TopWikisByCategoryLogic {
	return TopWikisByCategoryLogic{Data: data}
}

func (s TopWikisByCategoryLogic) ProcessTopWikisByCategoryLogic(parameters entities.TopWikisByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByCategoryResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopWikisByCategoryResponse{Items: make([]entities.TopWikisByCategory, 0)}

	scanner := s.Data.ProcessTopWikisByCategoryDataQuery(parameters, callContext)
	var wiki string
	var pageviewCount, rank int

	for scanner.Next() {
		if err = scanner.Scan(&wiki, &pageviewCount, &rank); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopWikisByCategoryResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopWikisByCategoryResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopWikisByCategory{
			Wiki:          wiki,
			PageviewCount: pageviewCount,
			Rank:          rank,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopWikisByCategoryResponse{}
	}

	return noProblem, response
}
