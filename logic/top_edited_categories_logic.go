package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopEditedCategoriesLogicInterface interface {
	ProcessTopEditedCategoriesLogic(parameters entities.TopEditedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditedCategoriesResponse)
}

type TopEditedCategoriesLogic struct {
	Data data.TopEditedCategoriesDataInterface
}

func NewTopEditedCategoriesLogic(data data.TopEditedCategoriesDataInterface) TopEditedCategoriesLogic {
	return TopEditedCategoriesLogic{Data: data}
}

func (s TopEditedCategoriesLogic) ProcessTopEditedCategoriesLogic(parameters entities.TopEditedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditedCategoriesResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopEditedCategoriesResponse{Items: make([]entities.TopEditedCategories, 0)}

	scanner := s.Data.ProcessTopEditedCategoriesDataQuery(parameters, callContext)
	var category string
	var editCount, rank int

	for scanner.Next() {
		if err = scanner.Scan(&category, &editCount, &rank); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopEditedCategoriesResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopEditedCategoriesResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopEditedCategories{
			Category:  category,
			EditCount: editCount,
			Rank:      rank,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopEditedCategoriesResponse{}
	}

	return noProblem, response
}
