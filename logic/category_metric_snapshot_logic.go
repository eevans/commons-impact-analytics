package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"
	"time"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type CategoryMetricLogicInterface interface {
	ProcessCategoryMetricLogic(parameters entities.CategoryMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryMetricResponse)
}

type CategoryMetricLogic struct {
	Data data.CategoryMetricDataInterface
}

func NewCategoryMetricLogic(data data.CategoryMetricDataInterface) CategoryMetricLogic {
	return CategoryMetricLogic{Data: data}
}

func (s CategoryMetricLogic) ProcessCategoryMetricLogic(parameters entities.CategoryMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryMetricResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.CategoryMetricResponse{Items: make([]entities.CategoryMetric, 0)}
	scanner := s.Data.ProcessCategoryMetricDataQuery(parameters, callContext)

	var dt time.Time
	var mediaFileCount, mediaFileCountDeep, usedMediaFileCount, usedMediaFileCountDeep, leveragingWikiCount, leveragingWikiCountDeep, leveragingPageCount, leveragingPageCountDeep int
	for scanner.Next() {
		if err = scanner.Scan(&dt, &mediaFileCount, &mediaFileCountDeep, &usedMediaFileCount, &usedMediaFileCountDeep, &leveragingWikiCount, &leveragingWikiCountDeep, &leveragingPageCount, &leveragingPageCountDeep); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.CategoryMetricResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.CategoryMetricResponse{}
			}
		}
		response.Items = append(response.Items, entities.CategoryMetric{
			Timestamp:               dt.String(),
			MediaFileCount:          mediaFileCount,
			MediaFileCountDeep:      mediaFileCountDeep,
			UsedMediaFileCount:      usedMediaFileCount,
			UsedMediaFileCountDeep:  usedMediaFileCountDeep,
			LeveragingWikiCount:     leveragingWikiCount,
			LeveragingWikiCountDeep: leveragingWikiCountDeep,
			LeveragingPageCount:     leveragingPageCount,
			LeveragingPageCountDeep: leveragingPageCountDeep,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.CategoryMetricResponse{}
	}
	if err := scanner.Err(); err != nil {
		aqsassist.LogQueryingDatabaseError(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryMetricResponse{}
	}
	return problemData, response
}
