package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopEditorsLogicInterface interface {
	ProcessTopEditorsLogic(parameters entities.TopEditorsParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditorsResponse)
}

type TopEditorsLogic struct {
	Data data.TopEditorsDataInterface
}

func NewTopEditorsLogic(data data.TopEditorsDataInterface) TopEditorsLogic {
	return TopEditorsLogic{Data: data}
}

func (s TopEditorsLogic) ProcessTopEditorsLogic(parameters entities.TopEditorsParameters, callContext entities.CallContext) (*problem.Problem, entities.TopEditorsResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopEditorsResponse{Items: make([]entities.TopEditors, 0)}

	scanner := s.Data.ProcessTopEditorsDataQuery(parameters, callContext)
	var userName string
	var editCount, rank int

	for scanner.Next() {
		if err = scanner.Scan(&userName, &editCount, &rank); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopEditorsResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopEditorsResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopEditors{
			UserName:  userName,
			EditCount: editCount,
			Rank:      rank,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopEditorsResponse{}
	}

	return noProblem, response
}
