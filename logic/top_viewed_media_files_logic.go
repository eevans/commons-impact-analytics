package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopViewedMediaFilesLogicInterface interface {
	ProcessTopViewedMediaFilesLogic(parameters entities.TopViewedMediaFilesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedMediaFilesResponse)
}

type TopViewedMediaFilesLogic struct {
	Data data.TopViewedMediaFilesDataInterface
}

func NewTopViewedMediaFilesLogic(data data.TopViewedMediaFilesDataInterface) TopViewedMediaFilesLogic {
	return TopViewedMediaFilesLogic{Data: data}
}

func (s TopViewedMediaFilesLogic) ProcessTopViewedMediaFilesLogic(parameters entities.TopViewedMediaFilesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedMediaFilesResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopViewedMediaFilesResponse{Items: make([]entities.TopViewedMediaFiles, 0)}

	scanner := s.Data.ProcessTopViewedMediaFilesDataQuery(parameters, callContext)
	var mediaFile string
	var pageviewCount, rank int

	for scanner.Next() {
		if err = scanner.Scan(&mediaFile, &pageviewCount, &rank); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopViewedMediaFilesResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopViewedMediaFilesResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopViewedMediaFiles{
			MediaFile:     mediaFile,
			PageviewCount: pageviewCount,
			Rank:          rank,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopViewedMediaFilesResponse{}
	}

	return noProblem, response
}
