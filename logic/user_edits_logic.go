package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"
	"time"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type UserEditsLogicInterface interface {
	ProcessUserEditsLogic(parameters entities.UserEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.UserEditsResponse)
}

type UserEditsLogic struct {
	Data data.UserEditsDataInterface
}

func NewUserEditsLogic(data data.UserEditsDataInterface) UserEditsLogic {
	return UserEditsLogic{Data: data}
}

func (s UserEditsLogic) ProcessUserEditsLogic(parameters entities.UserEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.UserEditsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.UserEditsResponse{Items: make([]entities.UserEdits, 0)}

	scanner := s.Data.ProcessUserEditsDataQuery(parameters, callContext)
	var timestamp time.Time
	var editCount int
	for scanner.Next() {
		if err = scanner.Scan(&timestamp, &editCount); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateUserNameNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.UserEditsResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.UserEditsResponse{}
			}
		}
		response.Items = append(response.Items, entities.UserEdits{
			Timestamp: timestamp.String(),
			EditCount: editCount,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateUserNameNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.UserEditsResponse{}
	}
	if err := scanner.Err(); err != nil {
		aqsassist.LogQueryingDatabaseError(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.UserEditsResponse{}
	}
	return problemData, response
}
