package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"
	"time"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type CategoryPageviewsLogicInterface interface {
	ProcessCategoryPageviewsLogic(parameters entities.CategoryPageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryPageviewsResponse)
}

type CategoryPageviewsLogic struct {
	Data data.CategoryPageviewsDataInterface
}

func NewCategoryPageviewsLogic(data data.CategoryPageviewsDataInterface) CategoryPageviewsLogic {
	return CategoryPageviewsLogic{Data: data}
}

func (s CategoryPageviewsLogic) ProcessCategoryPageviewsLogic(parameters entities.CategoryPageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryPageviewsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.CategoryPageviewsResponse{Items: make([]entities.CategoryPageviews, 0)}

	scanner := s.Data.ProcessCategoryPageviewsDataQuery(parameters, callContext)
	var timestamp time.Time
	var pageviewCount int
	for scanner.Next() {
		if err = scanner.Scan(&timestamp, &pageviewCount); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.CategoryPageviewsResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.CategoryPageviewsResponse{}
			}
		}
		response.Items = append(response.Items, entities.CategoryPageviews{
			Timestamp:     timestamp.String(),
			PageviewCount: pageviewCount,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.CategoryPageviewsResponse{}
	}
	if err := scanner.Err(); err != nil {
		aqsassist.LogQueryingDatabaseError(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryPageviewsResponse{}
	}
	return problemData, response
}
