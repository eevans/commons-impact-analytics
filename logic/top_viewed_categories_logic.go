package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopViewedCategoriesLogicInterface interface {
	ProcessTopViewedCategoriesLogic(parameters entities.TopViewedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedCategoriesResponse)
}

func NewTopViewedCategoriesLogic(data data.TopViewedCategoriesDataInterface) TopViewedCategoriesLogic {
	return TopViewedCategoriesLogic{Data: data}
}

type TopViewedCategoriesLogic struct {
	Data data.TopViewedCategoriesDataInterface
}

func (s TopViewedCategoriesLogic) ProcessTopViewedCategoriesLogic(parameters entities.TopViewedCategoriesParameters, callContext entities.CallContext) (*problem.Problem, entities.TopViewedCategoriesResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopViewedCategoriesResponse{Items: make([]entities.TopViewedCategories, 0)}

	scanner := s.Data.ProcessTopViewedCategoriesDataQuery(parameters, callContext)
	var category string
	var pageviewCount, rank int

	for scanner.Next() {
		if err = scanner.Scan(&category, &pageviewCount, &rank); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateYearMonthNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopViewedCategoriesResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopViewedCategoriesResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopViewedCategories{
			Category:      category,
			PageviewCount: pageviewCount,
			Rank:          rank,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateYearMonthNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopViewedCategoriesResponse{}
	}

	return noProblem, response
}
