package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"
	"time"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type CategoryEditsLogicInterface interface {
	ProcessCategoryEditsLogic(parameters entities.CategoryEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryEditsResponse)
}

func NewCategoryEditsLogic(data data.CategoryEditsDataInterface) CategoryEditsLogic {
	return CategoryEditsLogic{Data: data}
}

type CategoryEditsLogic struct {
	Data data.CategoryEditsDataInterface
}

func (s CategoryEditsLogic) ProcessCategoryEditsLogic(parameters entities.CategoryEditsParameters, callContext entities.CallContext) (*problem.Problem, entities.CategoryEditsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.CategoryEditsResponse{Items: make([]entities.CategoryEdits, 0)}
	scanner := s.Data.ProcessCategoryEditsDataQuery(parameters, callContext)
	var timestamp time.Time
	var editCount int
	for scanner.Next() {
		if err = scanner.Scan(&timestamp, &editCount); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.CategoryEditsResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.CategoryEditsResponse{}
			}
		}
		response.Items = append(response.Items, entities.CategoryEdits{
			Timestamp: timestamp.String(),
			EditCount: editCount,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.CategoryEditsResponse{}
	}
	if err := scanner.Err(); err != nil {
		aqsassist.LogQueryingDatabaseError(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.CategoryEditsResponse{}
	}
	return problemData, response
}
