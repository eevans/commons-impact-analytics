package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopWikisByMediaFileLogicInterface interface {
	ProcessTopWikisByMediaFileLogic(parameters entities.TopWikisByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByMediaFileResponse)
}

type TopWikisByMediaFileLogic struct {
	Data data.TopWikisByMediaFileDataInterface
}

func NewTopWikisByMediaFileLogic(data data.TopWikisByMediaFileDataInterface) TopWikisByMediaFileLogic {
	return TopWikisByMediaFileLogic{Data: data}
}

func (s TopWikisByMediaFileLogic) ProcessTopWikisByMediaFileLogic(parameters entities.TopWikisByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopWikisByMediaFileResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopWikisByMediaFileResponse{Items: make([]entities.TopWikisByMediaFile, 0)}

	scanner := s.Data.ProcessTopWikisByMediaFileDataQuery(parameters, callContext)
	var wiki string
	var pageviewCount, rank int

	for scanner.Next() {
		if err = scanner.Scan(&wiki, &pageviewCount, &rank); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopWikisByMediaFileResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopWikisByMediaFileResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopWikisByMediaFile{
			Wiki:          wiki,
			PageviewCount: pageviewCount,
			Rank:          rank,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopWikisByMediaFileResponse{}
	}

	return noProblem, response
}
