package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"
	"time"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MediaFileMetricsSnapshotLogicInterface interface {
	ProcessMediaFileMetricsLogic(parameters entities.MediaFileMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFileMetricsSnapshotResponse)
}

type MediaFileMetricsSnapshotLogic struct {
	Data data.MediaFileMetricsSnapshotDataInterface
}

func NewMediaFileMetricsSnapshotLogic(data data.MediaFileMetricsSnapshotDataInterface) MediaFileMetricsSnapshotLogic {
	return MediaFileMetricsSnapshotLogic{Data: data}
}

func (s MediaFileMetricsSnapshotLogic) ProcessMediaFileMetricsLogic(parameters entities.MediaFileMetricsSnapshotParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFileMetricsSnapshotResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.MediaFileMetricsSnapshotResponse{Items: make([]entities.MediaFileMetricsSnapshot, 0)}
	scanner := s.Data.ProcessMediaFileMetricsSnapshotDataQuery(entities.MediaFileMetricsSnapshotParameters{
		MediaFile: parameters.MediaFile,
		Start:     parameters.Start,
		End:       parameters.End,
	}, entities.CallContext{
		Context:    callContext.Context,
		Session:    callContext.Session,
		Logger:     callContext.Logger,
		RequestURL: callContext.RequestURL,
	})
	var timestamp time.Time
	var leveragingWikiCount, leveragingPageCount int
	for scanner.Next() {
		if err = scanner.Scan(&timestamp, &leveragingWikiCount, &leveragingPageCount); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.MediaFileMetricsSnapshotResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.MediaFileMetricsSnapshotResponse{}
			}
		}
		response.Items = append(response.Items, entities.MediaFileMetricsSnapshot{
			Timestamp:           timestamp.String(),
			LeveragingWikiCount: leveragingWikiCount,
			LeveragingPageCount: leveragingPageCount,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.MediaFileMetricsSnapshotResponse{}
	}
	if err := scanner.Err(); err != nil {
		aqsassist.LogQueryingDatabaseError(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.MediaFileMetricsSnapshotResponse{}
	}
	return problemData, response
}
