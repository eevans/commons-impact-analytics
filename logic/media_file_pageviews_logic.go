package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"
	"time"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MediaFilePageviewsLogicInterface interface {
	ProcessMediaFilePageviewsLogic(parameters entities.MediaFilePageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFilePageviewsResponse)
}

type MediaFilePageviewsLogic struct {
	Data data.MediaFilePageviewsDataInterface
}

func NewMediaFilePageviewsLogic(data data.MediaFilePageviewsDataInterface) MediaFilePageviewsLogic {
	return MediaFilePageviewsLogic{Data: data}
}

func (s MediaFilePageviewsLogic) ProcessMediaFilePageviewsLogic(parameters entities.MediaFilePageviewsParameters, callContext entities.CallContext) (*problem.Problem, entities.MediaFilePageviewsResponse) {
	var err error
	var problemData *problem.Problem
	var response = entities.MediaFilePageviewsResponse{Items: make([]entities.MediaFilePageviews, 0)}

	scanner := s.Data.ProcessMediaFilePageviewsDataQuery(parameters, callContext)
	var timestamp time.Time
	var pageviewCount int
	for scanner.Next() {
		if err = scanner.Scan(&timestamp, &pageviewCount); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.MediaFilePageviewsResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.MediaFilePageviewsResponse{}
			}
		}
		response.Items = append(response.Items, entities.MediaFilePageviews{
			Timestamp:     timestamp.String(),
			PageviewCount: pageviewCount,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.MediaFilePageviewsResponse{}
	}
	if err := scanner.Err(); err != nil {
		aqsassist.LogQueryingDatabaseError(callContext.Logger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
		return problemResp, entities.MediaFilePageviewsResponse{}
	}
	return problemData, response
}
