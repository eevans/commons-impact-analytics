package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopPagesByMediaFileLogicInterface interface {
	ProcessTopPagesByMediaFileLogic(parameters entities.TopPagesByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByMediaFileResponse)
}

type TopPagesByMediaFileLogic struct {
	Data data.TopPagesByMediaFileDataInterface
}

func NewTopPagesByMediaFileLogic(data data.TopPagesByMediaFileDataInterface) TopPagesByMediaFileLogic {
	return TopPagesByMediaFileLogic{Data: data}
}

func (s TopPagesByMediaFileLogic) ProcessTopPagesByMediaFileLogic(parameters entities.TopPagesByMediaFileParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByMediaFileResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopPagesByMediaFileResponse{Items: make([]entities.TopPagesByMediaFile, 0)}

	scanner := s.Data.ProcessTopPagesByMediaFileDataQuery(parameters, callContext)
	var pageTitle string
	var pageviewCount, rank int

	for scanner.Next() {
		if err = scanner.Scan(&pageTitle, &pageviewCount, &rank); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopPagesByMediaFileResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopPagesByMediaFileResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopPagesByMediaFile{
			PageTitle:     pageTitle,
			PageviewCount: pageviewCount,
			Rank:          rank,
		})
	}

	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateMediafileNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopPagesByMediaFileResponse{}
	}

	return noProblem, response
}
