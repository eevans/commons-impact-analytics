package logic

import (
	"common-impact-analytics/data"
	"common-impact-analytics/entities"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type TopPagesByCategoryLogicInterface interface {
	ProcessTopPagesByCategoryLogic(parameters entities.TopPagesByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByCategoryResponse)
}

func NewTopPagesByCategoryLogic(data data.TopPagesByCategoryDataInterface) TopPagesByCategoryLogic {
	return TopPagesByCategoryLogic{Data: data}
}

type TopPagesByCategoryLogic struct {
	Data data.TopPagesByCategoryDataInterface
}

func (s TopPagesByCategoryLogic) ProcessTopPagesByCategoryLogic(parameters entities.TopPagesByCategoryParameters, callContext entities.CallContext) (*problem.Problem, entities.TopPagesByCategoryResponse) {
	var err error
	var noProblem *problem.Problem
	var response = entities.TopPagesByCategoryResponse{Items: make([]entities.TopPagesByCategory, 0)}

	scanner := s.Data.ProcessTopPagesByCategoryDataQuery(parameters, callContext)
	var pageTitle string
	var rank, pageviewCount int

	for scanner.Next() {
		if err = scanner.Scan(&pageTitle, &rank, &pageviewCount); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
				return problemResp, entities.TopPagesByCategoryResponse{}
			} else {
				aqsassist.LogQueryFailed(callContext.Logger, err.Error())
				problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), callContext.RequestURL)
				return problemResp, entities.TopPagesByCategoryResponse{}
			}
		}
		response.Items = append(response.Items, entities.TopPagesByCategory{
			PageTitle:     pageTitle,
			PageviewCount: pageviewCount,
			Rank:          rank,
		})
	}
	if len(response.Items) == 0 {
		problemResp := aqsassist.CreateCategoryNotFoundProblem(callContext.RequestURL)
		return problemResp, entities.TopPagesByCategoryResponse{}
	}

	return noProblem, response
}
