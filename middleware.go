package main

import (
	"io"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

// SetSecurityHeaders Add security headers
func SetSecurityHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, _ := io.ReadAll(r.Body)
		aqsassist.SetSecurityHeaders(w, body)
		next.ServeHTTP(w, r)
	})
}

// ValidateParameters Validate and normalize certain names for all endpoints. A 400 Bad Request error will be thrown if any invalid character is present
func ValidateParameters(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		params := []string{"wiki", "original-wiki", "reporting-wiki", "page-title"}
		for _, param := range params {
			if vars[param] != "" {
				validParam, normalizedParam := aqsassist.ValidateProject(vars[param])
				if !validParam {
					problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidProjectCharactersMessage, r.URL.RequestURI())
					(*problemResp).WriteTo(w)
					return
				}
				vars[param] = normalizedParam
			}
		}
		next.ServeHTTP(w, r)
	})
}
