package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopWikisByCategoryDataInterface interface {
	ProcessTopWikisByCategoryDataQuery(parameters entities.TopWikisByCategoryParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopWikisByCategoryData struct{}

func (c TopWikisByCategoryData) ProcessTopWikisByCategoryDataQuery(p entities.TopWikisByCategoryParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT wiki, rank, pageview_count
		FROM "commons".top_wikis_per_category_monthly
		WHERE category = ?
		AND category_scope = ?
		AND year = ?
		AND month = ?
		`, p.Category, p.CategoryScope, p.Year, p.Month)

	return processQuery(query, q.Context)
}
