package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryEditsDataInterface interface {
	ProcessCategoryEditsDataQuery(parameters entities.CategoryEditsParameters, queryContext entities.CallContext) gocql.Scanner
}

type CategoryEditsData struct{}

func (c CategoryEditsData) ProcessCategoryEditsDataQuery(p entities.CategoryEditsParameters, q entities.CallContext) gocql.Scanner {
	start := aqsassist.GetTime(p.Start[0:4], p.Start[4:6], p.Start[6:8])
	end := aqsassist.GetTime(p.End[0:4], p.End[4:6], p.End[6:8])

	query := q.Session.Query(`
		SELECT dt,
			edit_count
		FROM "commons".edits_per_category_monthly WHERE category = ?
		AND category_scope = ? AND edit_type = ? AND dt>=? AND dt<?`, p.Category, p.CategoryScope, p.EditType, start, end)

	return processQuery(query, q.Context)
}
