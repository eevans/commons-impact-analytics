package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopPagesByMediaFileDataInterface interface {
	ProcessTopPagesByMediaFileDataQuery(parameters entities.TopPagesByMediaFileParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopPagesByMediaFileData struct{}

func (c TopPagesByMediaFileData) ProcessTopPagesByMediaFileDataQuery(p entities.TopPagesByMediaFileParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT page_title, rank, pageview_count
		FROM "commons".top_pages_per_media_file_monthly
		WHERE media_file = ?
		AND wiki = ?
		AND year = ?
		AND month = ?
		`, p.MediaFile, p.Wiki, p.Year, p.Month)

	return processQuery(query, q.Context)
}
