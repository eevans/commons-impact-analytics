package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopViewedCategoriesDataInterface interface {
	ProcessTopViewedCategoriesDataQuery(parameters entities.TopViewedCategoriesParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopViewedCategoriesData struct{}

func (c TopViewedCategoriesData) ProcessTopViewedCategoriesDataQuery(p entities.TopViewedCategoriesParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT  category, rank, pageview_count
		FROM "commons".top_viewed_categories_monthly
		WHERE category_scope = ?
		AND wiki = ?
		AND year = ?
		AND month = ?
		`, p.CategoryScope, p.Wiki, p.Year, p.Month)

	return processQuery(query, q.Context)
}
