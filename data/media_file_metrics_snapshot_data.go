package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type MediaFileMetricsSnapshotDataInterface interface {
	ProcessMediaFileMetricsSnapshotDataQuery(parameters entities.MediaFileMetricsSnapshotParameters, queryContext entities.CallContext) gocql.Scanner
}

type MediaFileMetricsSnapshotData struct{}

func (c MediaFileMetricsSnapshotData) ProcessMediaFileMetricsSnapshotDataQuery(p entities.MediaFileMetricsSnapshotParameters, q entities.CallContext) gocql.Scanner {
	start := aqsassist.GetTime(p.Start[0:4], p.Start[4:6], p.Start[6:8])
	end := aqsassist.GetTime(p.End[0:4], p.End[4:6], p.End[6:8])
	query := q.Session.Query(`
		SELECT dt,
			leveraging_wiki_count,
			leveraging_page_count
		FROM "commons".media_file_metrics_snapshot
		WHERE media_file = ?
		AND dt >= ?
		AND dt < ?`, p.MediaFile, start, end)

	return processQuery(query, q.Context)
}
