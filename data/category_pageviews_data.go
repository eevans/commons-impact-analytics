package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryPageviewsDataInterface interface {
	ProcessCategoryPageviewsDataQuery(parameters entities.CategoryPageviewsParameters, queryContext entities.CallContext) gocql.Scanner
}

type CategoryPageviewsData struct{}

func (c CategoryPageviewsData) ProcessCategoryPageviewsDataQuery(p entities.CategoryPageviewsParameters, q entities.CallContext) gocql.Scanner {
	start := aqsassist.GetTime(p.Start[0:4], p.Start[4:6], p.Start[6:8])
	end := aqsassist.GetTime(p.End[0:4], p.End[4:6], p.End[6:8])
	query := q.Session.Query(`
		SELECT dt,
			pageview_count
		FROM "commons".pageviews_per_category_monthly
		WHERE category = ?
		AND category_scope = ?
		AND wiki = ?
		AND dt >= ?
		AND dt < ?
		`, p.Category, p.CategoryScope, p.Wiki, start, end)

	return processQuery(query, q.Context)
}
