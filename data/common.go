package data

import (
	"context"
	"github.com/gocql/gocql"
)

func processQuery(query *gocql.Query, context context.Context) gocql.Scanner {
	scanner := query.WithContext(context).Iter().Scanner()
	return scanner
}
