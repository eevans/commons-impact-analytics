package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopPagesByCategoryDataInterface interface {
	ProcessTopPagesByCategoryDataQuery(parameters entities.TopPagesByCategoryParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopPagesByCategoryData struct{}

func (c TopPagesByCategoryData) ProcessTopPagesByCategoryDataQuery(p entities.TopPagesByCategoryParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT page_title, rank, pageview_count
		FROM "commons".top_pages_per_category_monthly
		WHERE category = ?
		AND category_scope = ?
		AND wiki = ?
		AND year = ?
		AND month = ?
		`, p.Category, p.CategoryScope, p.Wiki, p.Year, p.Month)

	return processQuery(query, q.Context)
}
