package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type UserEditsDataInterface interface {
	ProcessUserEditsDataQuery(parameters entities.UserEditsParameters, queryContext entities.CallContext) gocql.Scanner
}

type UserEditsData struct{}

func (c UserEditsData) ProcessUserEditsDataQuery(p entities.UserEditsParameters, q entities.CallContext) gocql.Scanner {
	start := aqsassist.GetTime(p.Start[0:4], p.Start[4:6], p.Start[6:8])
	end := aqsassist.GetTime(p.End[0:4], p.End[4:6], p.End[6:8])

	query := q.Session.Query(`
		SELECT dt,
			edit_count
		FROM "commons".edits_per_user_monthly
		WHERE user_name = ?
		AND edit_type = ?
		AND dt >= ?
		AND dt < ?`, p.UserName, p.EditType, start, end)

	return processQuery(query, q.Context)
}
