package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryMetricDataInterface interface {
	ProcessCategoryMetricDataQuery(parameters entities.CategoryMetricsSnapshotParameters, queryContext entities.CallContext) gocql.Scanner
}

type CategoryMetricData struct {
}

func (s CategoryMetricData) ProcessCategoryMetricDataQuery(p entities.CategoryMetricsSnapshotParameters, q entities.CallContext) gocql.Scanner {
	start := aqsassist.GetTime(p.Start[0:4], p.Start[4:6], p.Start[6:8])
	end := aqsassist.GetTime(p.End[0:4], p.End[4:6], p.End[6:8])
	query := q.Session.Query(`
		SELECT dt,
			media_file_count,
			media_file_count_deep,
			used_media_file_count,
			used_media_file_count_deep,
			leveraging_wiki_count,
			leveraging_wiki_count_deep,
			leveraging_page_count,
			leveraging_page_count_deep
		FROM "commons".category_metrics_snapshot
		WHERE "category" = ?
		AND dt >= ?
		AND dt < ?`, p.Category, start, end)

	return processQuery(query, q.Context)
}
