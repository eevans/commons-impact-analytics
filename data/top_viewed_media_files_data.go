package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopViewedMediaFilesDataInterface interface {
	ProcessTopViewedMediaFilesDataQuery(parameters entities.TopViewedMediaFilesParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopViewedMediaFilesData struct{}

func (c TopViewedMediaFilesData) ProcessTopViewedMediaFilesDataQuery(p entities.TopViewedMediaFilesParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT media_file, rank, pageview_count
		FROM "commons".top_viewed_media_files_monthly
		WHERE category = ?
		AND category_scope = ?
		AND wiki = ?
		AND year = ?
		AND month = ?
		`, p.Category, p.CategoryScope, p.Wiki, p.Year, p.Month)

	return processQuery(query, q.Context)
}
