package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopEditedCategoriesDataInterface interface {
	ProcessTopEditedCategoriesDataQuery(parameters entities.TopEditedCategoriesParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopEditedCategoriesData struct{}

func (c TopEditedCategoriesData) ProcessTopEditedCategoriesDataQuery(p entities.TopEditedCategoriesParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT category, rank, edit_count
		FROM "commons".top_edited_categories_monthly
		WHERE category_scope = ?
		AND edit_type = ?
		AND year = ?
		AND month = ?
		`, p.CategoryScope, p.EditType, p.Year, p.Month)

	return processQuery(query, q.Context)
}
