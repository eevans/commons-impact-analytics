package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopWikisByMediaFileDataInterface interface {
	ProcessTopWikisByMediaFileDataQuery(parameters entities.TopWikisByMediaFileParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopWikisByMediaFileData struct{}

func (c TopWikisByMediaFileData) ProcessTopWikisByMediaFileDataQuery(p entities.TopWikisByMediaFileParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT wiki,pageview_count,rank
		FROM "commons".top_wikis_per_media_file_monthly
		WHERE media_file = ?
		AND year = ?
		AND month = ?
		`, p.MediaFile, p.Year, p.Month)

	return processQuery(query, q.Context)
}
