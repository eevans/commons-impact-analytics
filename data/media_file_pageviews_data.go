package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type MediaFilePageviewsDataInterface interface {
	ProcessMediaFilePageviewsDataQuery(parameters entities.MediaFilePageviewsParameters, queryContext entities.CallContext) gocql.Scanner
}

type MediaFilePageviewsData struct{}

func (c MediaFilePageviewsData) ProcessMediaFilePageviewsDataQuery(p entities.MediaFilePageviewsParameters, q entities.CallContext) gocql.Scanner {
	start := aqsassist.GetTime(p.Start[0:4], p.Start[4:6], p.Start[6:8])
	end := aqsassist.GetTime(p.End[0:4], p.End[4:6], p.End[6:8])
	query := q.Session.Query(`
		SELECT dt,
			pageview_count
		FROM "commons".pageviews_per_media_file_monthly
		WHERE media_file = ?
		AND wiki = ?
		AND dt >= ?
		AND dt < ?
		`, p.MediaFile, p.Wiki, start, end)

	return processQuery(query, q.Context)
}
