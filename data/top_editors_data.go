package data

import (
	"common-impact-analytics/entities"

	"github.com/gocql/gocql"
)

type TopEditorsDataInterface interface {
	ProcessTopEditorsDataQuery(parameters entities.TopEditorsParameters, queryContext entities.CallContext) gocql.Scanner
}

type TopEditorsData struct{}

func (c TopEditorsData) ProcessTopEditorsDataQuery(p entities.TopEditorsParameters, q entities.CallContext) gocql.Scanner {

	query := q.Session.Query(`
		SELECT user_name, rank, edit_count
		FROM "commons".top_editors_monthly
		WHERE category = ?
		AND category_scope = ?
		AND edit_type = ?
		AND year = ?
		AND month = ?
		`, p.Category, p.CategoryScope, p.EditType, p.Year, p.Month)

	return processQuery(query, q.Context)
}
