*This page is a work in progress.*

Common Impact Analytics is a service that provides a public API developed and maintained by the Wikimedia Foundation that serves analytical data  on the impact of Community contributions to Commons. So far, the data is focused on media files uploaded by- and categories belonging to GLAM actors (affiliates, projects, individual contributors etc.).

Data provided by this API is available under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).
