package test

import (
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"net/http"
	"os"
	"testing"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockTopViewedMediaFilesData struct {
	mock.Mock
}

func (m *MockTopViewedMediaFilesData) ProcessTopViewedMediaFilesDataQuery(parameters entities.TopViewedMediaFilesParameters, queryContext entities.CallContext) gocql.Scanner {
	args := m.Called(parameters, queryContext)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessTopViewedMediaFilesLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockTopViewedMediaFilesData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	mediaFile := "test"
	pageviewCount := 47
	rank := 4
	expectedResponse := entities.TopViewedMediaFilesResponse{
		Items: []entities.TopViewedMediaFiles{
			{
				MediaFile:     mediaFile,
				PageviewCount: pageviewCount,
				Rank:          rank,
			},
		},
	}

	parameters := entities.TopViewedMediaFilesParameters{
		CategoryScope: "test",
		Category:      "test",
		Wiki:          "test",
		Year:          "2012",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{mediaFile, pageviewCount, rank},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopViewedMediaFilesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopViewedMediaFilesLogic(mockData)

	_, actualResponse := logic.ProcessTopViewedMediaFilesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessTopViewedMediaFilesLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockTopViewedMediaFilesData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	parameters := entities.TopViewedMediaFilesParameters{
		CategoryScope: "test",
		Wiki:          "test",
		Year:          "2012",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopViewedMediaFilesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopViewedMediaFilesLogic(mockData)

	_, actualResponse := logic.ProcessTopViewedMediaFilesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))

}

func TestProcessTopViewedMediaFilesLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockTopViewedMediaFilesData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080//metrics/common-impact-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	parameters := entities.TopViewedMediaFilesParameters{
		CategoryScope: "test",
		Wiki:          "test",
		Year:          "2012",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{23},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopViewedMediaFilesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopViewedMediaFilesLogic(mockData)

	pbmResp, actualResponse := logic.ProcessTopViewedMediaFilesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}
