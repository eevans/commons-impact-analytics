package test

import (
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"net/http"
	"os"
	"testing"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockTopPagesByMediaFileData struct {
	mock.Mock
}

func (m *MockTopPagesByMediaFileData) ProcessTopPagesByMediaFileDataQuery(parameters entities.TopPagesByMediaFileParameters, queryContext entities.CallContext) gocql.Scanner {
	args := m.Called(parameters, queryContext)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessTopPagesByMediaFileLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockTopPagesByMediaFileData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	pageTitle := "test"
	pageviewCount := 47
	rank := 4
	expectedResponse := entities.TopPagesByMediaFileResponse{
		Items: []entities.TopPagesByMediaFile{
			{
				PageTitle:     pageTitle,
				PageviewCount: pageviewCount,
				Rank:          rank,
			},
		},
	}

	parameters := entities.TopPagesByMediaFileParameters{
		MediaFile: "test",
		Wiki:      "test",
		Year:      "2012",
		Month:     "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{pageTitle, pageviewCount, rank},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopPagesByMediaFileDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopPagesByMediaFileLogic(mockData)

	_, actualResponse := logic.ProcessTopPagesByMediaFileLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessTopPagesByMediaFileLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockTopPagesByMediaFileData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	parameters := entities.TopPagesByMediaFileParameters{
		MediaFile: "test",
		Wiki:      "test",
		Year:      "2012",
		Month:     "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopPagesByMediaFileDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopPagesByMediaFileLogic(mockData)

	_, actualResponse := logic.ProcessTopPagesByMediaFileLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))

}

func TestProcessTopPagesByMediaFileLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockTopPagesByMediaFileData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080//metrics/common-impact-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	parameters := entities.TopPagesByMediaFileParameters{
		MediaFile: "test",
		Wiki:      "test",
		Year:      "2012",
		Month:     "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{1, 1},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopPagesByMediaFileDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopPagesByMediaFileLogic(mockData)

	pbmResp, actualResponse := logic.ProcessTopPagesByMediaFileLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}
