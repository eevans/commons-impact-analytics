package test

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/handlers"
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"net/http/httptest"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type MockCategoryEditsLogic struct {
	mock.Mock
}

func (m *MockCategoryEditsLogic) ProcessCategoryEditsLogic(params entities.CategoryEditsParameters, ctx entities.CallContext) (*problem.Problem, entities.CategoryEditsResponse) {
	expected := m.Called(params, ctx)
	return expected.Get(0).(*problem.Problem), expected.Get(1).(entities.CategoryEditsResponse)
}

func TestCategoryEditsHandler_HandleHTTP(t *testing.T) {
	var pbm = (*problem.Problem)(nil)
	category := "Library of Congress"
	categoryScope := "deep"
	editType := "create"
	start := "2000081000"
	end := "2007092100"
	mockLogger := &log.Logger{}
	session := &gocql.Session{}
	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.CategoryEditsParameters{
		Category:      category,
		CategoryScope: categoryScope,
		EditType:      editType,
		Start:         start,
		End:           end,
	}
	req = mux.SetURLVars(req, map[string]string{
		"category":       params.Category,
		"category-scope": params.CategoryScope,
		"edit-type":      params.EditType,
		"start":          params.Start,
		"end":            params.End,
	})

	mockResponse := entities.CategoryEditsResponse{
		Context: entities.CategoryEditsContext{
			Endpoint:      "commons-analytics/edits-per-category-monthly",
			Category:      category,
			CategoryScope: categoryScope,
			EditType:      editType,
			Start:         start,
			End:           end,
		},
		Items: []entities.CategoryEdits{
			{
				Timestamp: "2020031500",
				EditCount: 2019,
			}},
	}

	mockLogic := new(MockCategoryEditsLogic)

	handler := &handlers.CategoryEditsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	ctx := context.Background()
	callContext := entities.CallContext{
		Context:    ctx,
		Session:    session,
		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessCategoryEditsLogic", params, callContext).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.CategoryEditsResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessCategoryEditsLogic", params, callContext)
}

func TestCategoryEditsHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockCategoryEditsLogic)

	handler := &handlers.CategoryEditsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	url := testURL("/does/not/matter/params/set/below")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = url
	params := entities.CategoryEditsParameters{
		Category:      "Library of Congress",
		CategoryScope: "deep",
		EditType:      "create",
		Start:         "2000081000",
		End:           "2007092100",
	}
	req = mux.SetURLVars(req, map[string]string{
		"category":       params.Category,
		"category-scope": params.CategoryScope,
		"edit-type":      params.EditType,
		"start":          params.Start,
		"end":            params.End,
	})

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", req.URL.RequestURI())

	ctx := context.Background()
	callContext := entities.CallContext{
		Context:    ctx,
		Session:    session,
		Logger:     mockLogger.Request(req),
		RequestURL: req.URL.RequestURI(),
	}

	mockLogic.On("ProcessCategoryEditsLogic", params, callContext).
		Return(mockError, entities.CategoryEditsResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessCategoryEditsLogic", params, callContext)
}
