package test

import (
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"net/http"
	"os"
	"testing"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockTopEditedCategoriesData struct {
	mock.Mock
}

func (m *MockTopEditedCategoriesData) ProcessTopEditedCategoriesDataQuery(parameters entities.TopEditedCategoriesParameters, queryContext entities.CallContext) gocql.Scanner {
	args := m.Called(parameters, queryContext)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessTopEditedCategories_Success(t *testing.T) {
	// setup mock data
	mockData := &MockTopEditedCategoriesData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	category := "Bodies of water"
	editCount := 47
	rank := 4
	expectedResponse := entities.TopEditedCategoriesResponse{
		Items: []entities.TopEditedCategories{
			{
				Category:  category,
				EditCount: editCount,
				Rank:      rank,
			},
		},
	}

	parameters := entities.TopEditedCategoriesParameters{
		CategoryScope: "test",
		EditType:      "update",
		Year:          "2018",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{category, editCount, rank},
	})

	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopEditedCategoriesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopEditedCategoriesLogic(mockData)

	_, actualResponse := logic.ProcessTopEditedCategoriesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessTopEditedCategories_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockTopEditedCategoriesData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	parameters := entities.TopEditedCategoriesParameters{
		CategoryScope: "test",
		EditType:      "update",
		Year:          "2018",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)

	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopEditedCategoriesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopEditedCategoriesLogic(mockData)

	_, actualResponse := logic.ProcessTopEditedCategoriesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, len(actualResponse.Items), 0)

}

func TestProcessTopEditedCategories_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockTopEditedCategoriesData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080//metrics/common-impact-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	parameters := entities.TopEditedCategoriesParameters{
		CategoryScope: "test",
		EditType:      "update",
		Year:          "2018",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	topJson := "[{\"category\":\"Bodies of water\",\"edit-count\":\"47\",\"main_count\":47,\"rank\":4}]"

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{topJson},
	})

	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopEditedCategoriesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopEditedCategoriesLogic(mockData)

	pbmResp, actualResponse := logic.ProcessTopEditedCategoriesLogic(parameters, callContext)

	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}
