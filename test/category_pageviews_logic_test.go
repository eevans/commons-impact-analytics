package test

import (
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"net/http"
	"os"
	"testing"
	"time"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockCategoryPageviewsData struct {
	mock.Mock
}

func (m *MockCategoryPageviewsData) ProcessCategoryPageviewsDataQuery(parameters entities.CategoryPageviewsParameters, queryContext entities.CallContext) gocql.Scanner {
	args := m.Called(parameters, queryContext)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessCategoryPageviewsLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockCategoryPageviewsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	timestamp := time.Now()
	expectedResponse := entities.CategoryPageviewsResponse{
		Items: []entities.CategoryPageviews{
			{
				Timestamp:     timestamp.String(),
				PageviewCount: 47,
			},
		},
	}

	parameters := entities.CategoryPageviewsParameters{
		Category:      "test",
		CategoryScope: "test",
		Wiki:          "test",
		Start:         "start",
		End:           "end",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{timestamp, 47},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessCategoryPageviewsDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewCategoryPageviewsLogic(mockData)

	_, actualResponse := logic.ProcessCategoryPageviewsLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessCategoryPageviewsLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockCategoryPageviewsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	parameters := entities.CategoryPageviewsParameters{
		Category:      "test",
		CategoryScope: "test",
		Wiki:          "test",
		Start:         "start",
		End:           "end",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)

	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessCategoryPageviewsDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewCategoryPageviewsLogic(mockData)

	_, actualResponse := logic.ProcessCategoryPageviewsLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, len(actualResponse.Items), 0)

}

func TestProcessCategoryPageviewsLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockCategoryPageviewsData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080//metrics/common-impact-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	parameters := entities.CategoryPageviewsParameters{
		Category:      "test",
		CategoryScope: "test",
		Wiki:          "test",
		Start:         "start",
		End:           "end",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{"20201212", 47, 33},
	})

	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessCategoryPageviewsDataQuery", parameters, callContext).Return(mockScanner)

	logic := logic.NewCategoryPageviewsLogic(mockData)

	pbmResp, actualResponse := logic.ProcessCategoryPageviewsLogic(parameters, callContext)

	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}
