package test

import (
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"net/http"
	"os"
	"testing"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockTopViewedCategoriesData struct {
	mock.Mock
}

func (m *MockTopViewedCategoriesData) ProcessTopViewedCategoriesDataQuery(parameters entities.TopViewedCategoriesParameters, queryContext entities.CallContext) gocql.Scanner {
	args := m.Called(parameters, queryContext)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessTopViewedCategoriesLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockTopViewedCategoriesData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	category := "test"
	pageviewCount := 47
	rank := 4
	expectedResponse := entities.TopViewedCategoriesResponse{
		Items: []entities.TopViewedCategories{
			{
				Category:      category,
				PageviewCount: pageviewCount,
				Rank:          rank,
			},
		},
	}

	parameters := entities.TopViewedCategoriesParameters{
		CategoryScope: "test",
		Wiki:          "test",
		Year:          "2012",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{category, pageviewCount, rank},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopViewedCategoriesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopViewedCategoriesLogic(mockData)

	_, actualResponse := logic.ProcessTopViewedCategoriesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessTopViewedCategoriesLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockTopViewedCategoriesData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	parameters := entities.TopViewedCategoriesParameters{
		CategoryScope: "test",
		Wiki:          "test",
		Year:          "2012",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}
	mockScanner := new(MockScanner)
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopViewedCategoriesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopViewedCategoriesLogic(mockData)

	_, actualResponse := logic.ProcessTopViewedCategoriesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))

}

func TestProcessTopViewedCategoriesLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockTopViewedCategoriesData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080//metrics/common-impact-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	parameters := entities.TopViewedCategoriesParameters{
		CategoryScope: "test",
		Wiki:          "test",
		Year:          "2012",
		Month:         "12",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{22},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopViewedCategoriesDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopViewedCategoriesLogic(mockData)

	pbmResp, actualResponse := logic.ProcessTopViewedCategoriesLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}
