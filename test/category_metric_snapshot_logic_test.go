package test

import (
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"reflect"
	"testing"
	"time"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockCategoryMetricData struct {
	mock.Mock
}

type MockScanner struct {
	mock.Mock
	data  [][]interface{}
	index int
}

func (m *MockScanner) SetData(data [][]interface{}) {
	m.data = data
	m.index = -1
}

func (m *MockScanner) Scan(dest ...interface{}) error {
	if m.index >= len(m.data) {
		return io.EOF
	}
	row := m.data[m.index]
	if len(dest) != len(row) {
		return fmt.Errorf("number of columns in data row doesn't match number of destination variables")
	}
	for i, val := range row {
		destValue := reflect.ValueOf(dest[i])
		destType := destValue.Type()
		if destType.Kind() != reflect.Ptr {
			return fmt.Errorf("destination variable must be a pointer")
		}
		destValue.Elem().Set(reflect.ValueOf(val))
	}
	return nil
}

func (m *MockScanner) Err() error {
	return nil
}

func (m *MockScanner) Next() bool {
	m.index++
	return m.index < len(m.data)
}

func (m *MockCategoryMetricData) ProcessCategoryMetricDataQuery(parameters entities.CategoryMetricsSnapshotParameters, queryContext entities.CallContext) gocql.Scanner {
	args := m.Called(parameters, queryContext)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessCategoryMetricLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockCategoryMetricData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	timeStamp := time.Now()
	expectedResponse := entities.CategoryMetricResponse{
		Items: []entities.CategoryMetric{
			{
				Timestamp:               timeStamp.String(),
				MediaFileCount:          0,
				MediaFileCountDeep:      0,
				UsedMediaFileCount:      0,
				UsedMediaFileCountDeep:  0,
				LeveragingWikiCount:     0,
				LeveragingWikiCountDeep: 0,
				LeveragingPageCount:     0,
				LeveragingPageCountDeep: 0,
			},
		},
	}

	parameters := entities.CategoryMetricsSnapshotParameters{
		Category: "test",
		Start:    "start",
		End:      "end",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{timeStamp, 0, 0, 0, 0, 0, 0, 0, 0},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.AnythingOfType("time.Time"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int")).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessCategoryMetricDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewCategoryMetricLogic(mockData)

	_, actualResponse := logic.ProcessCategoryMetricLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessCategoryMetricLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockCategoryMetricData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	parameters := entities.CategoryMetricsSnapshotParameters{
		Category: "test",
		Start:    "start",
		End:      "end",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)

	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int")).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessCategoryMetricDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewCategoryMetricLogic(mockData)

	_, actualResponse := logic.ProcessCategoryMetricLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, len(actualResponse.Items), 0)

}

func TestProcessCategoryMetricLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockCategoryMetricData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080//metrics/common-impact-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	parameters := entities.CategoryMetricsSnapshotParameters{
		Category: "test",
		Start:    "start",
		End:      "end",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{0, 0, 0, 0, 0, 0, 0},
	})

	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int"), mock.AnythingOfType("*int")).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessCategoryMetricDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewCategoryMetricLogic(mockData)

	pbmResp, actualResponse := logic.ProcessCategoryMetricLogic(parameters, callContext)

	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}
