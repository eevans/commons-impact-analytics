package test

import (
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"net/http"
	"os"
	"testing"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockTopEditorsData struct {
	mock.Mock
}

func (m *MockTopEditorsData) ProcessTopEditorsDataQuery(parameters entities.TopEditorsParameters, queryContext entities.CallContext) gocql.Scanner {
	args := m.Called(parameters, queryContext)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessTopEditorsLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockTopEditorsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}
	userName := "Milimetric"
	editCount := 47
	rank := 4
	expectedResponse := entities.TopEditorsResponse{
		Items: []entities.TopEditors{
			{
				UserName:  userName,
				EditCount: editCount,
				Rank:      rank,
			},
		},
	}

	parameters := entities.TopEditorsParameters{
		Category:      "test",
		CategoryScope: "test",
		EditType:      "test",
		Year:          "test",
		Month:         "test",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}

	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{userName, editCount, rank},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopEditorsDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopEditorsLogic(mockData)

	_, actualResponse := logic.ProcessTopEditorsLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)

}

func TestProcessTopEditorsLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockTopEditorsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	parameters := entities.TopEditorsParameters{
		Category:      "test",
		CategoryScope: "test",
		EditType:      "test",
		Year:          "test",
		Month:         "test",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}
	mockScanner := new(MockScanner)
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopEditorsDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopEditorsLogic(mockData)

	_, actualResponse := logic.ProcessTopEditorsLogic(parameters, callContext)

	// assert response matches expected value
	assert.Equal(t, len(actualResponse.Items), 0)

}

func TestProcessTopEditorsLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockTopEditorsData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080//metrics/common-impact-analytics", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)
	parameters := entities.TopEditorsParameters{
		Category:      "test",
		CategoryScope: "test",
		EditType:      "test",
		Year:          "test",
		Month:         "test",
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    session,
		Logger:     rLogger,
		RequestURL: "",
	}
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{1},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessTopEditorsDataQuery", parameters, callContext).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewTopEditorsLogic(mockData)

	pbmResp, actualResponse := logic.ProcessTopEditorsLogic(parameters, callContext)

	assert.Equal(t, 0, len(actualResponse.Items))
	assert.NotEmpty(t, pbmResp.Error())

}
