package entities

type TopWikisByCategoryParameters struct {
	Category      string
	CategoryScope string
	Year          string
	Month         string
}

type TopWikisByCategoryContext struct {
	Endpoint      string `json:"endpoint" example:"2024010100"`
	Category      string `json:"category" example:"Museum of Modern Art (New York City)"`
	CategoryScope string `json:"category-scope" example:"shallow|deep"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"03"`
}

type TopWikisByCategoryResponse struct {
	Context TopWikisByCategoryContext `json:"context"`
	Items   []TopWikisByCategory      `json:"items"`
}

type TopWikisByCategory struct {
	Wiki          string `json:"wiki" example:"et.wikipedia"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}
