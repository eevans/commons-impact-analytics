package entities

type CategoryMetricsSnapshotParameters struct {
	Category string
	Start    string
	End      string
}
type CategoryMetricsSnapshotContext struct {
	Endpoint string `json:"endpoint" example:"2024010100"`
	Category string `json:"category" example:"Museum of Modern Art (New York City)"`
	Start    string `json:"start" example:"2024010100"`
	End      string `json:"end" example:"2024010100"`
}

type CategoryMetricResponse struct {
	Context CategoryMetricsSnapshotContext `json:"context"`
	Items   []CategoryMetric               `json:"items"`
}

type CategoryMetric struct {
	Timestamp               string `json:"timestamp" example:"2024010100"`
	MediaFileCount          int    `json:"media-file-count" example:"12345"`
	MediaFileCountDeep      int    `json:"media-file-count-deep" example:"1234567"`
	UsedMediaFileCount      int    `json:"used-media-file-count" example:"453"`
	UsedMediaFileCountDeep  int    `json:"used-media-file-count-deep" example:"4536"`
	LeveragingWikiCount     int    `json:"leveraging-wiki-count" example:"12"`
	LeveragingWikiCountDeep int    `json:"leveraging-wiki-count-deep" example:"120"`
	LeveragingPageCount     int    `json:"leveraging-page-count" example:"47"`
	LeveragingPageCountDeep int    `json:"leveraging-page-count-deep" example:"47"`
}
