package entities

type TopViewedCategoriesParameters struct {
	CategoryScope string
	Wiki          string
	Year          string
	Month         string
}

type TopViewedCategoriesContext struct {
	Endpoint      string `json:"endpoint" example:"2024010100"`
	CategoryScope string `json:"category-scope" example:"shallow|deep"`
	Wiki          string `json:"wiki" example:"et.wikipedia|ar.wiktionary|...|all-wikis"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"03"`
}

// For top endpoints, data is stored in json and parsed by the logic layer
// NOTE: this is different from previous AQS "tops" endpoints where,
// for example, "countries" was nested within the first item in "items" TODO: confirm this is intended
type TopViewedCategoriesResponse struct {
	Context TopViewedCategoriesContext `json:"context"`
	Items   []TopViewedCategories      `json:"items"`
}

type TopViewedCategories struct {
	Category      string `json:"category" example:"Bodies of Water"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}
