package entities

type MediaFileMetricsSnapshotParameters struct {
	MediaFile string
	Start     string
	End       string
}

type MediaFileMetricsSnapshotContext struct {
	Endpoint  string `json:"endpoint" example:"TODO?"`
	MediaFile string `json:"media-file" example:"Duck.jpg"`
	Start     string `json:"start" example:"2024010100"`
	End       string `json:"end" example:"2024020100"`
}

type MediaFileMetricsSnapshotResponse struct {
	Context MediaFileMetricsSnapshotContext `json:"context"`
	Items   []MediaFileMetricsSnapshot      `json:"items"`
}

type MediaFileMetricsSnapshot struct {
	Timestamp           string `json:"timestamp" example:"2024010100"`
	LeveragingWikiCount int    `json:"leveraging-wiki-count" example:"12"`
	LeveragingPageCount int    `json:"leveraging-page-count" example:"47"`
}
