package entities

import (
	"context"
	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
)

type CallContext struct {
	Context    context.Context
	Session    *gocql.Session
	Logger     *logger.RequestScopedLogger
	RequestURL string
}
