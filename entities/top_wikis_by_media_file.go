package entities

type TopWikisByMediaFileParameters struct {
	MediaFile string
	Year      string
	Month     string
}

type TopWikisByMediaFileContext struct {
	Endpoint  string `json:"endpoint" example:"2024010100"`
	MediaFile string `json:"media-file" example:"Duck.jpg"`
	Year      string `json:"year" example:"2024"`
	Month     string `json:"month" example:"03"`
}

type TopWikisByMediaFileResponse struct {
	Context TopWikisByMediaFileContext `json:"context"`
	Items   []TopWikisByMediaFile      `json:"items"`
}

type TopWikisByMediaFile struct {
	Wiki          string `json:"wiki" example:"et.wikipedia"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}
