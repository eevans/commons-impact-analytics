package entities

type TopPagesByMediaFileParameters struct {
	MediaFile string
	Wiki      string
	Year      string
	Month     string
}

type TopPagesByMediaFileContext struct {
	Endpoint  string `json:"endpoint" example:"2024010100"`
	MediaFile string `json:"media-file" example:"Duck.jpg"`
	Wiki      string `json:"wiki" example:"et.wikipedia|ar.wiktionary|...|all-wikis"`
	Year      string `json:"year" example:"2024"`
	Month     string `json:"month" example:"03"`
}

type TopPagesByMediaFileResponse struct {
	Context TopPagesByMediaFileContext `json:"context"`
	Items   []TopPagesByMediaFile      `json:"items"`
}

type TopPagesByMediaFile struct {
	PageTitle     string `json:"page-title" example:"Lake"`
	PageviewCount int    `json:"pageview-count" example:"47"`
	Rank          int    `json:"rank" example:"4"`
}
