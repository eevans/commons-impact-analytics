package entities

type TopViewedMediaFilesParameters struct {
	Category      string
	CategoryScope string
	Wiki          string
	Year          string
	Month         string
}

type TopViewedMediaFilesContext struct {
	Endpoint      string `json:"endpoint" example:"2024010100"`
	Category      string `json:"category" example:"Museum of Modern Art (New York City)"`
	CategoryScope string `json:"category-scope" example:"shallow|deep"`
	Wiki          string `json:"wiki" example:"et.wikipedia|ar.wiktionary|...|all-wikis"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"03"`
}

type TopViewedMediaFilesResponse struct {
	Context TopViewedMediaFilesContext `json:"context"`
	Items   []TopViewedMediaFiles      `json:"items"`
}

type TopViewedMediaFiles struct {
	MediaFile     string `json:"media-file" example:"Pond.jpg"`
	PageviewCount int    `json:"pageview-count" example:"46"`
	Rank          int    `json:"rank" example:"4"`
}
