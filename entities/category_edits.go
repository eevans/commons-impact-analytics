package entities

type CategoryEditsParameters struct {
	Category      string
	CategoryScope string
	EditType      string
	Start         string
	End           string
}

type CategoryEditsContext struct {
	Endpoint      string `json:"endpoint" example:"TODO?"`
	Category      string `json:"category" example:"Museum of Modern Art (New York City)"`
	CategoryScope string `json:"category-scope" example:"shallow|deep"`
	EditType      string `json:"edit-type" example:"create|update|all-edit-types"`
	Start         string `json:"start" example:"2024010100"`
	End           string `json:"end" example:"2024020100"`
}

type CategoryEditsResponse struct {
	Context CategoryEditsContext `json:"context"`
	Items   []CategoryEdits      `json:"items"`
}

type CategoryEdits struct {
	Timestamp string `json:"timestamp" example:"2024010100"`
	EditCount int    `json:"edit-count" example:"47"`
}
