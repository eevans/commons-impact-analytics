package entities

type CategoryPageviewsParameters struct {
	Category      string
	CategoryScope string
	Wiki          string
	Start         string
	End           string
}

type CategoryPageviewsContext struct {
	Endpoint      string `json:"endpoint" example:"2024010100"`
	Category      string `json:"category" example:"Museum of Modern Art (New York City)"`
	CategoryScope string `json:"category-scope" example:"shallow|deep"`
	Wiki          string `json:"wiki" example:"et.wikipedia|ar.wiktionary|...|all-wikis"`
	Start         string `json:"start" example:"2024010100"`
	End           string `json:"end" example:"2024020100"`
}

type CategoryPageviewsResponse struct {
	Context CategoryPageviewsContext `json:"context"`
	Items   []CategoryPageviews      `json:"items"`
}

type CategoryPageviews struct {
	Timestamp     string `json:"timestamp" example:"2024010100"`
	PageviewCount int    `json:"pageview-count" example:"47"`
}
