package entities

type UserEditsParameters struct {
	UserName string
	EditType string
	Start    string
	End      string
}

type UserEditsContext struct {
	Endpoint string `json:"end-point" example:"commons-analytics/edits-per-user-monthly"`
	UserName string `json:"user-name" example:"xyzz"`
	EditType string `json:"edit-type" example:"create"`
	Start    string `json:"start" example:"2024010100"`
	End      string `json:"end" example:"2024020100"`
}

type UserEditsResponse struct {
	Context UserEditsContext `json:"context"`
	Items   []UserEdits      `json:"items"`
}

type UserEdits struct {
	Timestamp string `json:"timestamp" example:"2024010100"`
	EditCount int    `json:"edit-count" example:"47"`
}
