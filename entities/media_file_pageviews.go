package entities

type MediaFilePageviewsParameters struct {
	MediaFile string
	Wiki      string
	Start     string
	End       string
}

type MediaFilePageviewsContext struct {
	Endpoint  string `json:"endpoint" example:"2024010100"`
	MediaFile string `json:"media-file" example:"Duck.jpg"`
	Wiki      string `json:"wiki" example:"et.wikipedia|ar.wiktionary|...|all-wikis"`
	Start     string `json:"start" example:"2024010100"`
	End       string `json:"end" example:"2024020100"`
}

type MediaFilePageviewsResponse struct {
	Context MediaFilePageviewsContext `json:"context"`
	Items   []MediaFilePageviews      `json:"items"`
}

type MediaFilePageviews struct {
	Timestamp     string `json:"timestamp" example:"2024010100"`
	PageviewCount int    `json:"pageview-count" example:"47"`
}
