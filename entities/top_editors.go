package entities

type TopEditorsParameters struct {
	Category      string
	CategoryScope string
	EditType      string
	Year          string
	Month         string
}

type TopEditorsContext struct {
	Endpoint      string `json:"endpoint" example:"2024010100"`
	Category      string `json:"category" example:"Bodies of Water"`
	CategoryScope string `json:"category-scope" example:"shallow|deep"`
	EditType      string `json:"edit-type" example:"create|update"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"03"`
}

type TopEditorsResponse struct {
	Context TopEditorsContext `json:"context"`
	Items   []TopEditors      `json:"items"`
}

type TopEditors struct {
	UserName  string `json:"user-name" example:"Milimetric"`
	EditCount int    `json:"edit-count" example:"47"`
	Rank      int    `json:"rank" example:"4"`
}
