package entities

type TopEditedCategoriesParameters struct {
	CategoryScope string
	EditType      string
	Year          string
	Month         string
}

type TopEditedCategoriesContext struct {
	Endpoint      string `json:"endpoint" example:"2024010100"`
	CategoryScope string `json:"category-scope" example:"shallow|deep"`
	EditType      string `json:"edit-type" example:"create|update"`
	Year          string `json:"year" example:"2024"`
	Month         string `json:"month" example:"03"`
}

type TopEditedCategoriesResponse struct {
	Context TopEditedCategoriesContext `json:"context"`
	Items   []TopEditedCategories      `json:"items"`
}

type TopEditedCategories struct {
	Category  string `json:"category" example:"Bodies of Water"`
	EditCount int    `json:"edit-count" example:"47"`
	Rank      int    `json:"rank" example:"4"`
}
