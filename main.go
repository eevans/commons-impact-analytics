package main

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/data"
	"common-impact-analytics/handlers"
	"common-impact-analytics/logic"
	"flag"
	"fmt"
	"net/http"
	"os"
	"path"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	prometheusmiddleware "github.com/albertogviana/prometheus-middleware"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

var (
	// These values are assigned at build using `-ldflags` (see: Makefile)
	buildDate = "unknown"
	buildHost = "unknown"
	version   = "unknown"
)

// API documentation
//	@title					Wikimedia Common Impact Metrics API
//	@version				DRAFT
//	@description.markdown	api.md
//	@contact.name
//	@contact.url
//	@contact.email
//	@termsOfService	https://wikimediafoundation.org/wiki/Terms_of_Use
//	@host			localhost:8080
//	@basePath		/metrics/
//	@schemes		http

// Entrypoint for the service
func main() {
	var confFile = flag.String("config", "./config.yaml", aqsassist.ConfigurationPathMessage)

	var config *configuration.Config
	var err error
	var categoryMetricData data.CategoryMetricData
	var categoryMetricLogic = logic.NewCategoryMetricLogic(categoryMetricData)
	var categoryEditsData data.CategoryEditsData
	var categoryEditsLogic = logic.NewCategoryEditsLogic(categoryEditsData)
	var mediaFileMetricsSnapshotData data.MediaFileMetricsSnapshotData
	var mediaFileMetricsSnapshotLogic = logic.NewMediaFileMetricsSnapshotLogic(mediaFileMetricsSnapshotData)
	var mediaFilePageviewsData data.MediaFilePageviewsData
	var mediaFilePageviewsLogic = logic.NewMediaFilePageviewsLogic(mediaFilePageviewsData)
	var categoryPageviewsData data.CategoryPageviewsData
	var categoryPageviewsLogic = logic.NewCategoryPageviewsLogic(categoryPageviewsData)
	var userEditsData data.UserEditsData
	var userEditsLogic = logic.NewUserEditsLogic(userEditsData)
	var topPagesByCategoryData data.TopPagesByCategoryData
	var topPagesByCategoryLogic = logic.NewTopPagesByCategoryLogic(topPagesByCategoryData)
	var topWikisByCategoryData data.TopWikisByCategoryData
	var topWikisByCategoryLogic = logic.NewTopWikisByCategoryLogic(topWikisByCategoryData)
	var topPagesByMediaFileData data.TopPagesByMediaFileData
	var topPagesByMediaFileLogic = logic.NewTopPagesByMediaFileLogic(topPagesByMediaFileData)
	var topViewedCategoriesData data.TopViewedCategoriesData
	var topViewedCategoriesLogic = logic.NewTopViewedCategoriesLogic(topViewedCategoriesData)
	var topWikisByMediaFileData data.TopWikisByMediaFileData
	var topWikisByMediaFileLogic = logic.NewTopWikisByMediaFileLogic(topWikisByMediaFileData)
	var topViewedMediaFilesData data.TopViewedMediaFilesData
	var topViewedMediaFilesLogic = logic.NewTopViewedMediaFilesLogic(topViewedMediaFilesData)
	var topEditedCategoriesData data.TopEditedCategoriesData
	var topEditedCategoriesLogic = logic.NewTopEditedCategoriesLogic(topEditedCategoriesData)
	var topEditorsData data.TopEditorsData
	var topEditorsLogic = logic.NewTopEditorsLogic(topEditorsData)

	flag.Parse()

	if config, err = configuration.ReadConfig(*confFile); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	logger, err := log.NewLogger(os.Stdout, config.ServiceName, config.LogLevel)
	if err != nil {
		fmt.Fprintf(os.Stderr, aqsassist.ErrorInitializingLoggerMessage, err)
		os.Exit(1)
	}

	// Allow overriding config using environment variables
	MergeEnvironment(config, logger)

	aqsassist.LogCassandraServiceInitialization(logger, config.ServiceName, version, buildHost, buildDate, config.Cassandra.Hosts,
		config.Cassandra.Port, config.Cassandra.Consistency, config.Cassandra.LocalDC)

	session, err := newCassandraSession(config)
	if err != nil {
		logger.Error(aqsassist.CassandraConnectionErrorMessage, err)
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	// pass bound struct method to handler
	categoryMetricSnapshotHandler := &handlers.CategoryMetricSnapshotHandler{
		Logger: logger, Session: session, Logic: &categoryMetricLogic, Config: config}
	categoryPageviewsHandler := &handlers.CategoryPageviewsHandler{
		Logger: logger, Session: session, Logic: &categoryPageviewsLogic, Config: config}
	mediafileMetricsSnapshotHandler := &handlers.MediafileMetricsSnapshotHandler{
		Logger: logger, Session: session, Logic: &mediaFileMetricsSnapshotLogic, Config: config}
	mediaFilePageviewsHandler := &handlers.MediaFilePageviewsHandler{
		Logger: logger, Session: session, Logic: &mediaFilePageviewsLogic, Config: config}
	categoryEditsHandler := &handlers.CategoryEditsHandler{
		Logger: logger, Session: session, Logic: &categoryEditsLogic, Config: config}
	userEditsHandler := &handlers.UserEditsHandler{
		Logger: logger, Session: session, Logic: &userEditsLogic, Config: config}
	topPagesByCategoryHandler := &handlers.TopPagesByCategoryHandler{
		Logger: logger, Session: session, Logic: &topPagesByCategoryLogic, Config: config}
	topWikisByCategoryHandler := &handlers.TopWikisByCategoryHandler{
		Logger: logger, Session: session, Logic: &topWikisByCategoryLogic, Config: config}
	topViewedCategoriesHandler := &handlers.TopViewedCategoriesHandler{
		Logger: logger, Session: session, Logic: &topViewedCategoriesLogic, Config: config}
	topPagesByMediaFileHandler := &handlers.TopPagesByMediaFileHandler{
		Logger: logger, Session: session, Logic: &topPagesByMediaFileLogic, Config: config}
	topWikisByMediaFileHandler := &handlers.TopWikisByMediaFileHandler{
		Logger: logger, Session: session, Logic: &topWikisByMediaFileLogic, Config: config}
	topViewedMediaFilesHandler := &handlers.TopViewedMediaFilesHandler{
		Logger: logger, Session: session, Logic: &topViewedMediaFilesLogic, Config: config}
	topEditedCategoriesHandler := &handlers.TopEditedCategoriesHandler{
		Logger: logger, Session: session, Logic: &topEditedCategoriesLogic, Config: config}
	topEditorsHandler := &handlers.TopEditorsHandler{
		Logger: logger, Session: session, Logic: &topEditorsLogic, Config: config}

	healthz := NewHealthz(version, buildDate, buildHost)
	middleware := prometheusmiddleware.NewPrometheusMiddleware(prometheusmiddleware.Opts{})

	r := mux.NewRouter().SkipClean(true).UseEncodedPath()
	r.NotFoundHandler = http.HandlerFunc(handlers.NotFoundHandler)
	p := promhttp.Handler()

	r.Use(SetSecurityHeaders, ValidateParameters, middleware.InstrumentHandlerDuration)

	r.Handle("/metrics", p).Methods("GET")
	r.HandleFunc("/healthz", healthz.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/category-metrics-snapshot/{category}/{start}/{end}"), categoryMetricSnapshotHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/media-file-metrics-snapshot/{media-file}/{start}/{end}"), mediafileMetricsSnapshotHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/pageviews-per-category-monthly/{category}/{category-scope}/{wiki}/{start}/{end}"), categoryPageviewsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/pageviews-per-media-file-monthly/{media-file}/{wiki}/{start}/{end}"), mediaFilePageviewsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/edits-per-category-monthly/{category}/{category-scope}/{edit-type}/{start}/{end}"), categoryEditsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/edits-per-user-monthly/{user-name}/{edit-type}/{start}/{end}"), userEditsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-pages-per-category-monthly/{category}/{category-scope}/{wiki}/{year}/{month}"), topPagesByCategoryHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-wikis-per-category-monthly/{category}/{category-scope}/{year}/{month}"), topWikisByCategoryHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-viewed-categories-monthly/{category-scope}/{wiki}/{year}/{month}"), topViewedCategoriesHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-pages-per-media-file-monthly/{media-file}/{wiki}/{year}/{month}"), topPagesByMediaFileHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-wikis-per-media-file-monthly/{media-file}/{year}/{month}"), topWikisByMediaFileHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-viewed-media-files-monthly/{category}/{category-scope}/{wiki}/{year}/{month}"), topViewedMediaFilesHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-editors-monthly/{category}/{category-scope}/{edit-type}/{year}/{month}"), topEditorsHandler.HandleHTTP).Methods("GET")
	r.HandleFunc(path.Join(config.BaseURI, "/top-edited-categories-monthly/{category-scope}/{edit-type}/{year}/{month}"), topEditedCategoriesHandler.HandleHTTP).Methods("GET")

	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", config.Address, config.Port),
		Handler: r,
	}

	err = srv.ListenAndServe()
	fmt.Println(err.Error())
}
