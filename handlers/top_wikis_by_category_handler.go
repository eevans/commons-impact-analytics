package handlers

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopWikisByCategoryHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.TopWikisByCategoryLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//		@summary		Gets rank of wikis with their aggregated pageviews for a given category/category tree and time interval
//		@router			/commons-analytics/top-wikis-per-category-monthly/{category}/{category-scope}/{year}/{month} [get]
//		@description	Given a category and a date range, returns the number of edits for that category.
//	    @param			category		path	string	true	"The name of the desired category. Coincides with the page title of the category page in Commons."	                     example(Museum of Us)
//		@param			category-scope	path	string	true	"Single category rank (shallow) or a top-level category tree rank (deep)"	                                         example(shallow)
//		@param			year		    path	string	true	"First date to include, in YYYY format"	                                                                                 example(2022)
//		@param			month			path	string	true	"Last date to include, in YMM format"	                                                                                 example(07)
//		@produce		json
//		@success		200	{object}	entities.TopWikisByCategoryResponse
//		@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//		@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//		@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopWikisByCategoryHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	year := vars["year"]
	month := vars["month"]
	category := vars["category"]
	// TODO: decide how we want to validate things like category-scope that would normally be enums
	categoryScope := vars["category-scope"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopWikisByCategoryParameters{
		Category:      category,
		CategoryScope: categoryScope,
		Year:          year,
		Month:         month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopWikisByCategoryLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopWikisByCategoryContext{
		Endpoint:      "commons-analytics/top-wikis-per-category-monthly",
		Category:      category,
		CategoryScope: categoryScope,
		Year:          year,
		Month:         month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
