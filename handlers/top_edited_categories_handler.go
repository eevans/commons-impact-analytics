package handlers

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopEditedCategoriesHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.TopEditedCategoriesLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//	@summary		Gets time series of uploads and updates for a given category/deep category-tree and time interval
//	@router			/commons-analytics/top-edited-categories-monthly/{category-scope}/{edit-type}/{year}/{month} [get]
//	@description	Given a category and a date range, returns the number of edits for that category.
//	@param			category		    path	string	true	"The name of the desired category. Coincides with the page title of the category page in Commons"	    example(Museum of Modern Art (New York City))
//	@param			category-scope		path	string	true	"Single category rank (shallow) or a top-level category tree rank (deep)"	                        example(deep)
//	@param			edit-type		    path	string	true	"The type of page revision - create/update/all-edit-types"	                                            example(20220101)
//	@param			year		        path	string	true	"Year, in YYYY format"	                                                                                example(2000)
//	@param			month			    path	string	true	"Month,in MM format"	                                                                                example(02)
//	@produce		json
//	@success		200	{object}	entities.TopEditedCategoriesResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopEditedCategoriesHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error
	year := vars["year"]
	month := vars["month"]
	// TODO: decide how we want to validate things like category-scope that would normally be enums
	categoryScope := vars["category-scope"]
	editType := vars["edit-type"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsEditValue(editType) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidEditTypeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopEditedCategoriesParameters{
		CategoryScope: categoryScope,
		EditType:      editType,
		Year:          year,
		Month:         month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopEditedCategoriesLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopEditedCategoriesContext{
		Endpoint:      "commons-analytics/top-edited-categories-monthly",
		CategoryScope: categoryScope,
		EditType:      editType,
		Year:          year,
		Month:         month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
