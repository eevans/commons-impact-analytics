package handlers

import (
	"net/http"

	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

// NotFoundHandler is the HTTP handler when no match routes are found.

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	problemResp := aqsassist.CreateInvalidRouteProblem(r.URL.RequestURI())
	(*problemResp).WriteTo(w)
}
