package handlers

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryMetricSnapshotHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.CategoryMetricLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//	@summary		Gets time series of media files,levaraging wikis and articles for a given category/category-tree and time interval.
//	@router			/commons-analytics/category-metrics-snapshot/{category}/{start}/{end}  [get]
//	@description	Given a category and a date range, returns the number and time series of media file , levaraging wikis and articles.
//	@param			category	path	string	true	"Category of requested data"				example(Museum of Modern Art (New York City)
//	@param			start		path	string	true	"First date to include, in YYYYMMDD format"	example(20220101)
//	@param			end			path	string	true	"Last date to include, in YYYYMMDD format"	example(20220108)
//	@produce		json
//	@success		200	{object}	entities.CategoryMetricResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *CategoryMetricSnapshotHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	category := vars["category"]
	start := strings.ToLower(vars["start"])
	end := strings.ToLower(vars["end"])

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	var err error

	if start, err = aqsassist.ValidateTimestamp(start); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if end, err = aqsassist.ValidateTimestamp(end); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDEndDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	err = aqsassist.StartBeforeEnd(start, end)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.CategoryMetricsSnapshotParameters{
		Category: category,
		Start:    start,
		End:      end,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessCategoryMetricLogic(params, callContext)

	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.CategoryMetricsSnapshotContext{
		Endpoint: "commons-analytics/category-metrics-snapshot",
		Category: category,
		Start:    start,
		End:      end,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
