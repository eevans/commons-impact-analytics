package handlers

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopViewedCategoriesHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.TopViewedCategoriesLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//		@summary		Gets rank of category/category-trees with most page views for a given month or time interval
//		@router			/commons-analytics/top-viewed-categories-monthly/{category-scope}/{wiki}/{year}/{month} [get]
//		@description	Given a category and a time range, returns the rank of wikis with their aggregated pageviews.
//	    @param			category		path	string	true	"The name of the desired category. Coincides with the page title of the category page in Commons."	                     example(Museum of Us)
//		@param			category-scope	path	string	true	"Single category rank (shallow) or a top-level category tree rank (deep)"	                                         example(shallow)
//		@param			wiki		    path	string	true	"The wiki the user wants metrics about. If “all-wikis” is given, the metrics will show aggregate numbers for all wikis"	 example(all-wikis)
//		@param			year		    path	string	true	"The year to show data for, in YYYY."	                                                                                 example(2022)
//		@param			month			path	string	true	"The month to show data for, in MM."	                                                                                 example(12)
//		@produce		json
//		@success		200	{object}	entities.TopViewedCategoriesResponse
//		@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//		@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//		@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopViewedCategoriesHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	year := vars["year"]
	month := vars["month"]
	// TODO: decide how we want to validate things like category-scope that would normally be enums
	categoryScope := vars["category-scope"]
	wiki := vars["wiki"]
	var err error

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopViewedCategoriesParameters{
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Year:          year,
		Month:         month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopViewedCategoriesLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopViewedCategoriesContext{
		Endpoint:      "commons-analytics/top-viewed-categories-monthly",
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Year:          year,
		Month:         month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
