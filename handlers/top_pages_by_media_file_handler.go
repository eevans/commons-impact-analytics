package handlers

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopPagesByMediaFileHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.TopPagesByMediaFileLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//	@summary		Gets rank of pages with their aggregated views for a given media file and time interval
//	@router			/commons-analytics/top-pages-per-media-file-monthly/{media-file}/{wiki}/{year}/{month} [get]
//	@description	Given a category and a date range, returns the number of edits for that category.
//	@param			media-file		    path	string	true	"The name of the requested media-file. Coincides with the page title of the category page in Commons"	 example(20220101)
//	@param			wiki		        path	string	true	"The wiki the user wants metrics about."                                                                example(et.wikipedi)
//	@param			year			    path	string	true	"Year in YYYY format"	                                                                                 example(2022)
//	@param			month			    path	string	true	"Month in MM format"	                                                                                 example(07)
//	@produce		json
//	@success		200	{object}	entities.TopPagesByMediaFileResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopPagesByMediaFileHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	year := vars["year"]
	month := vars["month"]
	mediaFile := vars["media-file"]
	wiki := vars["wiki"]
	var err error

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopPagesByMediaFileParameters{
		MediaFile: mediaFile,
		Wiki:      wiki,
		Year:      year,
		Month:     month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessTopPagesByMediaFileLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopPagesByMediaFileContext{
		Endpoint:  "commons-analytics/top-pages-per-media-file-monthly",
		MediaFile: mediaFile,
		Wiki:      wiki,
		Year:      year,
		Month:     month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
