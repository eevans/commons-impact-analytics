package handlers

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type CategoryPageviewsHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.CategoryPageviewsLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//	@summary		Gets time series of pageviews for a given category /deep category-tree and time interval
//	@router			/commons-analytics/pageviews-per-category-monthly/{category}/{category-scope}/{edit-type}/{start}/{end} [get]
//	@description	Given a category and a date range, returns the number of pageviews for that category.
//	@param			start		path	string	true	"First date to include, in YYYYMMDD format"	example(20220101)
//	@param			end			path	string	true	"Last date to include, in YYYYMMDD format"	example(20220108)
//	@param			category	path	string	true	"The category of requested data"	        example(Museum of Modern Art (New York City)
//	@produce		json
//	@success		200	{object}	entities.CategoryPageviewsResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *CategoryPageviewsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error

	start := vars["start"]
	end := vars["end"]
	category := vars["category"]
	categoryScope := vars["category-scope"]
	wiki := vars["wiki"]
	// NOTE: removed pageTitle as an input per discussion last week, here and everywhere

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	if start, err = aqsassist.ValidateTimestamp(start); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if end, err = aqsassist.ValidateTimestamp(end); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDEndDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	err = aqsassist.StartBeforeEnd(start, end)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.CategoryPageviewsParameters{
		Category:      category,
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Start:         start,
		End:           end,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessCategoryPageviewsLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.CategoryPageviewsContext{
		Endpoint:      "commons-analytics/pageviews-per-category-monthly",
		Category:      category,
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Start:         start,
		End:           end,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
