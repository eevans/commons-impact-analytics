package handlers

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type MediaFilePageviewsHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.MediaFilePageviewsLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//	@summary		Gets time series of pageviews for a given media-file and time interval
//	@router			/commons-analytics/pageviews-per-media-file-monthly/{media-file}/{wiki}/{start}/{end} [get]
//	@description	Given a media-file and date range, returns the number of pageviews for that wiki.
//	@param			media-file	path	string	true	"The name of the requested media file."  	example(Duck.jpg)
//	@param			wiki		path	string	true	"The wiki the user wants metrics about."	example(et.wikipedia)
//	@param			start		path	string	true	"First date to include, in YYYYMM format"	example(202201)
//	@param			end			path	string	true	"Last date to include, in YYYYMM format"	example(202208)
//	@produce		json
//	@success		200	{object}	entities.MediaFilePageviewsResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *MediaFilePageviewsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	start := vars["start"]
	end := vars["end"]
	mediaFile := vars["media-file"]
	wiki := vars["wiki"]
	// NOTE: removed pageTitle as an input per discussion last week, here and everywhere

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)
	var err error

	if start, err = aqsassist.ValidateTimestamp(start); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDStartDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if end, err = aqsassist.ValidateTimestamp(end); err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidDDEndDateMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	err = aqsassist.StartBeforeEnd(start, end)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.MediaFilePageviewsParameters{
		MediaFile: mediaFile,
		Wiki:      wiki,
		Start:     start,
		End:       end,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	pbm, response := s.Logic.ProcessMediaFilePageviewsLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.MediaFilePageviewsContext{
		Endpoint:  "commons-analytics/pageviews-per-media-file-monthly",
		MediaFile: mediaFile,
		Wiki:      wiki,
		Start:     start,
		End:       end,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
