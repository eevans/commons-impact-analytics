package handlers

import (
	"common-impact-analytics/configuration"
	"common-impact-analytics/entities"
	"common-impact-analytics/logic"
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

type TopPagesByCategoryHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.TopPagesByCategoryLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//	@summary		Gets rank of pages with their aggregated views for a given category/deep category-tree and time interval
//	@router			/commons-analytics/top-pages-per-category-monthly/{category}/{category-scope}/{wiki}/{year}/{month} [get]
//	@description	Given a category and a date range, returns the number of edits for that category.
//	@param			category		    path	string	true	"The name of the desired category. Coincides with the page title of the category page in Commons"	example(Museum of Modern Art (New York City))
//	@param			category-scope		path	string	true	"Single category rank (shallow) or a top-level category tree rank (deep)"	                    example(deep)
//	@param			wiki		        path	string	true	"The wiki the user wants metrics about."                                                           example(et.wikipedi)
//	@param			year			    path	string	true	"Year in YYYY format"	                                                                            example(2022)
//	@param			month			    path	string	true	"Month in MM format"	                                                                            example(07)
//	@produce		json
//	@success		200	{object}	entities.TopPagesByCategoryResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *TopPagesByCategoryHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var err error
	year := vars["year"]
	month := vars["month"]
	category := vars["category"]
	// TODO: decide how we want to validate things like category-scope that would normally be enums
	categoryScope := vars["category-scope"]
	wiki := vars["wiki"]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	year, month, err = aqsassist.ValidateYearMonth(year, month)
	if err != nil {
		problemResp := aqsassist.CreateBadRequestProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	params := entities.TopPagesByCategoryParameters{
		Category:      category,
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Year:          year,
		Month:         month,
	}

	callContext := entities.CallContext{
		Context:    context.Background(),
		Session:    s.Session,
		Logger:     reqLogger,
		RequestURL: reqUrl,
	}

	if !aqsassist.IsCategoryScope(categoryScope) {
		problemResp := aqsassist.CreateBadRequestProblem(aqsassist.InvalidCategoryScopeMessage, reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	pbm, response := s.Logic.ProcessTopPagesByCategoryLogic(params, callContext)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	response.Context = entities.TopPagesByCategoryContext{
		Endpoint:      "commons-analytics/top-pages-per-category-monthly",
		Category:      category,
		CategoryScope: categoryScope,
		Wiki:          wiki,
		Year:          year,
		Month:         month,
	}

	var data []byte
	if data, err = json.Marshal(response); err != nil {
		aqsassist.LogMarshalError(reqLogger, err.Error())
		problemResp := aqsassist.CreateInternalServerErrorProblem(err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)

}
